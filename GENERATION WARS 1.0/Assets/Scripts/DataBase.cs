using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "Inventory System/Create Database", order = 1)]
public class DataBase : ScriptableObject
{
    public List<Item> items = new List<Item>();

}

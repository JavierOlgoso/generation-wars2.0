using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointerRaycast : MonoBehaviour
{
    public Camera mainCamera;
    public InventoryManager inventory;
    private void Update()
    {
        
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, Mathf.Infinity))
            {
                if(hit.collider.GetComponent<InteractiveItem>() != null)
                {
                    InteractiveItem i = hit.collider.GetComponent<InteractiveItem>();
                    inventory.AddToInventory(i.id, i.quantity);
                    Destroy(hit.collider.gameObject);
                }
            }
        }
    }
}

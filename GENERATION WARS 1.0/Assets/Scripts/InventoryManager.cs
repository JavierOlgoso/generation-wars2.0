using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryManager  : MonoBehaviour
{
    #region Singleton
    public static InventoryManager inventoryManager;
    

    private void Awake()
    {
        if (inventoryManager == null)
        {
            inventoryManager = this;
            DontDestroyOnLoad(this);
        
        }
        else
        {
            Destroy(this);
        }
    }
    public int spaceInventory = 10;
    [System.Serializable]
    public struct objectID
    {
        public int id;
        public int quantity;

        public objectID(int id, int quantity)
        {
            this.id = id;
            this.quantity = quantity;
        }
    }
    #endregion
    public List<objectID> inventory; 
    
    public void AddToInventory(int id, int quantity)
    {
        if(inventory.Count < spaceInventory)
        {
            inventory.Add(new objectID(id, quantity));
            Debug.Log("Add to inventory");
            Debug.Log(id);
            Debug.Log(quantity);
        }
    }
}
